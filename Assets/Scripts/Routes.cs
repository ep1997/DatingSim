﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Routes : MonoBehaviour
{
    public string MYRESPONSE = "it was really nice to meet you that night...";

    public static string currentPos = "char";
    //Clickable Choices throughout the conversation
    public string charA = "Favorite Color?";
    public string charB = "Favorite Song?";
    public string charC = "Favorite Food?";

    public string charAA = "Why do you like it?";
    public string charAB = "I prefer blue..";
    public string charAC = "Thats my favorite too";

    public string charAAA = "Youre interesting";
    public string charAAB = "Because its the color of ur eyes";
    public string charAAC = "Wut a coincidence roigt?";

    public string charABA = "such a cool color";
    public string charABB = "lol";
    public string charABC = "i really like u";

    public string charACA = "U have plans for tuesday?";
    public string charACB = "Im so tired";
    public string charACC = "You are the best m´lady";

    public string charBA = "Isnt that a little mainstream?";
    public string charBB = "I love that one!";
    public string charBC = "Boring choice";

    public string charBAA = "I was just kidding";
    public string charBAB = "dont get mad";
    public string charBAC = "rofl";

    public string charBBA = "We fit so well haha";
    public string charBBB = "sorry..im just nervous";
    public string charBBC = "i have to go now";

    public string charBCA = "I am sorry";
    public string charBCB = "no its not like that";
    public string charBCC = "dont go";

    public string charCA = "Then you should try the fried version aswell";
    public string charCB = "Ewww";
    public string charCC = "Then I have to try that soon";

    public string charCAA = "oh well..";
    public string charCAB = "i didnt know that";
    public string charCAC = "partayy";

    public string charCBA = "great";
    public string charCBB = "we have to go there sometime";
    public string charCBC = "im blushing";

    public string charCCA = "no";
    public string charCCB = "yep";
    public string charCCC = "maybe..";

    //Responses throughout the conversation by AI

    //A
    public string responseA = "Green";
    public string responseB = "September by earth, wind and fire";
    public string responseC = "Pizza";

    public string responseAA = "Its the color of the woods, I love to go there";
    public string responseAB = "Good for u..";
    public string responseAC = "You know whats good";

    public string responseAAA = "hehe";
    public string responseABA = "i think so too";
    public string responseACA = "the ocean is pretty nice as well";

    public string responseAAB = "why are you like that?";
    public string responseABB = "not really i guess";
    public string responseACB = "you dont even know me";

    public string responseAAC = "I cant explain it";
    public string responseABC = "you could be right";
    public string responseACC = "nice! i love that";

    //B
    public string responseBA = "oh really..";
    public string responseBB = "have you heard of bts then?";
    public string responseBC = "lolol";

    public string responseBAA = "i like cats";
    public string responseBBA = "i have an allergy";
    public string responseBCA = "pretty rude of u";

    public string responseBAB = "i dont know what to type";
    public string responseBBB = "did you get sick";
    public string responseBCB = "oh nope";

    public string responseBAC = "thats impressive";
    public string responseBBC = "i am sleepy";
    public string responseBCC = "i guess";

    //C
    public string responseCA = "havent heard of it";
    public string responseCB = "hihi, i gotta show u sometime";
    public string responseCC = "hmmmm";

    public string responseCAA = "i like dogs";
    public string responseCBA = "why is that?";
    public string responseCCA = "cats are cool as well";

    public string responseCAB = "thats boring";
    public string responseCBB = "have to go to the doctor tomorrow";
    public string responseCCB = "thank u";

    public string responseCAC = "i have to go now im sorry";
    public string responseCBC = "dont say that";
    public string responseCCC = "no...";



    public Transform AText;
    public Transform BText;
    public Transform CText;

    public Transform AI;
	// Use this for initialization
	void Start ()
    {
        AText.GetComponent<TextMesh>().text = charA;
        BText.GetComponent<TextMesh>().text = charB;
        CText.GetComponent<TextMesh>().text = charC;

        AI.GetComponent<TextMesh>().text = "What do you want to ask?";
    }

    public void OnMouseDown()
    {

        currentPos += gameObject.name;
        Debug.Log(currentPos);
        //A
        if (currentPos == "charA")
        {
            AI.GetComponent<TextMesh>().text = responseA;
            MYRESPONSE = responseA;

            AText.GetComponent<TextMesh>().text = charAA;
            BText.GetComponent<TextMesh>().text = charAB;
            CText.GetComponent<TextMesh>().text = charAC;
        }

        if (currentPos == "charAA")
        {
            AI.GetComponent<TextMesh>().text = responseAA;
            MYRESPONSE = responseAA;

            AText.GetComponent<TextMesh>().text = charAAA;
            BText.GetComponent<TextMesh>().text = charAAB;
            CText.GetComponent<TextMesh>().text = charAAC;
        }

        if (currentPos == "charAB")
        {
            AI.GetComponent<TextMesh>().text = responseAB;
            MYRESPONSE = responseAB;

            AText.GetComponent<TextMesh>().text = charABA;
            BText.GetComponent<TextMesh>().text = charABB;
            CText.GetComponent<TextMesh>().text = charABC;
        }

        if (currentPos == "charAC")
        {
            AI.GetComponent<TextMesh>().text = responseAC;

            AText.GetComponent<TextMesh>().text = charACA;
            BText.GetComponent<TextMesh>().text = charACB;
            CText.GetComponent<TextMesh>().text = charACC;
        }

        //B
        if (currentPos == "charB")
        {
            AI.GetComponent<TextMesh>().text = responseB;

            AText.GetComponent<TextMesh>().text = charBA;
            BText.GetComponent<TextMesh>().text = charBB;
            CText.GetComponent<TextMesh>().text = charBC;
        }

        if (currentPos == "charBA")
        {
            AI.GetComponent<TextMesh>().text = responseBA;

            AText.GetComponent<TextMesh>().text = charBAA;
            BText.GetComponent<TextMesh>().text = charBAB;
            CText.GetComponent<TextMesh>().text = charBAC;
        }

        if (currentPos == "charBB")
        {
            AI.GetComponent<TextMesh>().text = responseBB;

            AText.GetComponent<TextMesh>().text = charBBA;
            BText.GetComponent<TextMesh>().text = charBBB;
            CText.GetComponent<TextMesh>().text = charBBC;
        }

        if (currentPos == "charBC")
        {
            AI.GetComponent<TextMesh>().text = responseBC;

            AText.GetComponent<TextMesh>().text = charBCA;
            BText.GetComponent<TextMesh>().text = charBCB;
            CText.GetComponent<TextMesh>().text = charBCC;
        }

        //C
        if (currentPos == "charC")
        {
            AI.GetComponent<TextMesh>().text = responseB;

            AText.GetComponent<TextMesh>().text = charCA;
            BText.GetComponent<TextMesh>().text = charCB;
            CText.GetComponent<TextMesh>().text = charCC;
        }

        if (currentPos == "charCA")
        {
            AI.GetComponent<TextMesh>().text = responseCA;

            AText.GetComponent<TextMesh>().text = charCAA;
            BText.GetComponent<TextMesh>().text = charCAB;
            CText.GetComponent<TextMesh>().text = charCAC;
        }

        if (currentPos == "charCB")
        {
            AI.GetComponent<TextMesh>().text = responseCB;

            AText.GetComponent<TextMesh>().text = charCBA;
            BText.GetComponent<TextMesh>().text = charCBB;
            CText.GetComponent<TextMesh>().text = charCBC;
        }

        if (currentPos == "charCC")
        {
            AI.GetComponent<TextMesh>().text = responseCC;

            AText.GetComponent<TextMesh>().text = charCCA;
            BText.GetComponent<TextMesh>().text = charCCB;
            CText.GetComponent<TextMesh>().text = charCCC;
        }
    }
}
