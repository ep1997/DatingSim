﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class NPC_Bubbles : MonoBehaviour
{
    public static NPC_Bubbles myBubbles;

    public string[] CurrentDialogue;
    public string[] DialogueA;
    public string[] DialogueB;
    public string[] DialogueAA;
    public string[] DialogueBB;

    public string[] DialogueAB;
    public string[] DialogueBA;

    public string[] DialogueAAA;
    public string[] DialogueAAB;
    public string[] DialogueABB;
    public string[] DialogueABA;

    public string[] DialogueBBB;
    public string[] DialogueBBA;
    public string[] DialogueBAA;
    public string[] DialogueBAB;

    public string[] DialogueAAAA;
    public string[] DialogueABAA;
    public string[] DialogueABBA;
    public string[] DialogueAABB;
    public string[] DialogueAABA;
    public string[] DialogueABBB;
    public string[] DialogueAAAB;
    public string[] DialogueABAB;



    GameObject NPC_Bubble;
    public Transform spawnPoint;
    public Transform heartsSpawnpoint;
    public GameObject Canvas_NPC;
    public string currentText;
    public int textIndex = 0;

    public string currentPlayerString;

    public string AnswerA;
    public string AnswerAA;
    public string AnswerB;
    public string AnswerBB;
    public string AnswerAB;
    public string AnswerBA;

    public string AnswerABA;
    public string AnswerABB;
    public string AnswerBBA;
    public string AnswerBAA;
    public string AnswerBAB;
    public string AnswerAAB;
    public string AnswerAAA;
    public string AnswerBBB;

    public string AnswerAAAA;
    public string AnswerABAA;
    public string AnswerABBA;
    public string AnswerAABB;
    public string AnswerAABA;
    public string AnswerABBB;
    public string AnswerAAAB;
    public string AnswerABAB;

    public string AnswerBBBB;
    public string AnswerBBBA;
    public string AnswerBBAA;
    public string AnswerBBAB;
    public string AnswerBAAA;
    public string AnswerBAAB;
    public string AnswerBABA;
    public string AnswerBABB;


    public GameObject AnswerButton;
    public GameObject EndButton;

    GameObject HeartPrefab;

    public RectTransform rt;

    public GameObject left;

    public Canvas Canvas1;
    public ScrollRect myScroll;

    public bool myEnd;
    public string currentAI;

    void Start ()
    {
        rt = left.GetComponent<RectTransform>();
        myScroll = Canvas1.GetComponent<ScrollRect>();
        Canvas_NPC = GameObject.FindGameObjectWithTag("Canvas_NPC");
        myScroll.verticalNormalizedPosition = 0;

    }

    public void AddHeart()
    {
        switch(currentAI)
        {
        case "Jeon":
                SaveGame.savegame.heartsJeon += 1;
                HeartPrefab = Instantiate(Resources.Load("JeonHeart", typeof(GameObject))) as GameObject;
                break;
        case "Tae":
                SaveGame.savegame.heartsTae += 1;
                HeartPrefab = Instantiate(Resources.Load("TaeHeart", typeof(GameObject))) as GameObject;
                break;
        default:
                print("Incorrect string.");
                break;
        }


        HeartPrefab.transform.SetParent(heartsSpawnpoint, false);
        SaveGame.savegame.Save();

    }

    public void BeginChat()
    {
        StartCoroutine("Delay");
    }





    IEnumerator Delay()
    {
        foreach (var t in CurrentDialogue)
        {
            yield return new WaitForSeconds(1);
            currentText = CurrentDialogue[textIndex];
            SaveGame.savegame.currentPreviewString = CurrentDialogue[textIndex];
            if(currentAI == "Jeon")
            {
                NPC_Bubble = Instantiate(Resources.Load("JeonSpeechBubble", typeof(GameObject))) as GameObject;
            }
            else
            {
                NPC_Bubble = Instantiate(Resources.Load("TaeSpeechBubble", typeof(GameObject))) as GameObject;
            }
            NPC_Bubble.transform.SetParent(spawnPoint, false);
            //left.transform.position = new Vector3(left.transform.position.x, left.transform.position.y + 90, left.transform.position.z);
            //left.GetComponent<Rect>().height += 10;

            rt.sizeDelta = new Vector2(rt.sizeDelta.x, rt.sizeDelta.y + 200);

            myScroll.verticalNormalizedPosition = 0;

            textIndex++;
            yield return new WaitForSeconds(1);
        }
        textIndex = 0;

        if(myEnd == false)
        {
            AnswerButton.SetActive(true);
        }
        else
        {
            EndButton.SetActive(true);
        }
    }

    public void Endbutton()
    {
        SaveGame.savegame.chatsComplete += 1;
        SaveGame.savegame.Save();
        myRoutes.currentPos = "question";
        SceneManager.LoadScene("MainMenu");
    }

    public void PlayerChat()
    {
        currentText = currentPlayerString;
        SaveGame.savegame.currentPreviewString = currentPlayerString;
        NPC_Bubble = Instantiate(Resources.Load("NewPlayerBubble", typeof(GameObject))) as GameObject;
        NPC_Bubble.transform.SetParent(spawnPoint, false);
    }
}
