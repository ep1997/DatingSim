﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InterfaceScript : MonoBehaviour {

    public Text currentPreviewString;
	// Use this for initialization
	public void OnEnable () {
        SaveGame.savegame.Save();
        SaveGame.savegame.Load();
        UpdatePreview();
	}
	
    public void UpdatePreview()
    {
        if(SaveGame.savegame.currentPreviewString == "")
        {
            currentPreviewString.text = "Wow, you look pretty. Please come closer.";
        }
        else
        {
            currentPreviewString.text = SaveGame.savegame.currentPreviewString;
        }

        //if the current string is too long then show nearly as much as fits the rect + "..."
        //if (currentPreviewString.text.Length >= 15)
        //{
        //    currentPreviewString.text.Length = 12 + "...";
        //}
    }


}
