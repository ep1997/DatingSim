﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class myRoutes : MonoBehaviour
{
    public static myRoutes myroutes;

    public GameObject NPC_Manager;
    public NPC_Bubbles myNPC_Bubbles;
    public GameObject A;
    public GameObject B;

    public static string currentPos = "question";

    public string QuestionA;
    public string QuestionB;

    public string QuestionAA = "Would you like to go swimming?";
    public string QuestionBB = "How about a horror movie?";

    public string QuestionAB = "So what are we gonna do?";
    public string QuestionBA = "I am sorry for being rude..how about dinner?";

    public string QuestionAAA = "I really love swimming too!";
    public string QuestionBBB = "I think you are overreacting..jeez";



    public GameObject Choices;
    public GameObject Answer_Button;

    public Text childText;

    // Use this for initialization
    void Start ()
    {
        A = GameObject.FindGameObjectWithTag("ChoiceA");
        B = GameObject.FindGameObjectWithTag("ChoiceB");

        NPC_Manager = GameObject.FindGameObjectWithTag("NPC_Manager");
        myNPC_Bubbles = NPC_Manager.GetComponent<NPC_Bubbles>();



        A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerA;
        B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerB;

        if(childText.text == "")
        {
            gameObject.SetActive(false);
        }
    }

    public bool endOfChat = false;

    public void OnMyButtonPressed()
    {        
        currentPos += gameObject.name;



        switch (currentPos)
        {
            case "questionA":
                //A.transform.GetChild(0).GetComponent<Text>().text = QuestionAA;
                //B.transform.GetChild(0).GetComponent<Text>().text = QuestionBB;

                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueA;
                StartCoroutine("PlayerAnswering");
                NPC_Manager.GetComponent<NPC_Bubbles>().AddHeart();
                break;
            case "questionB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueB;
                myNPC_Bubbles.BeginChat();
                break;
            case "questionAA":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueAA;
                StartCoroutine("PlayerAnswering");
                NPC_Manager.GetComponent<NPC_Bubbles>().AddHeart();
                break;
            case "questionBB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBB;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionAB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueAB;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionBA":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBA;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionAAA":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueAAA;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionAAB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAABA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAABB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueAAB;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionABA":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueABA;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionABB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABBA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABBB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerABB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueABB;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionBBB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBBA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBBB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBBB;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionBBA":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBBA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBBA;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionBAA":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBAAA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBAAB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBAA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBAA;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionBAB":
                A.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBABA;
                B.transform.GetChild(0).GetComponent<Text>().text = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBABB;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerBAB;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBAB;
                StartCoroutine("PlayerAnswering");
                break;
            case "questionAAAA":
                endOfChat = true;
                NPC_Manager.GetComponent<NPC_Bubbles>().myEnd = true;

                NPC_Manager.GetComponent<NPC_Bubbles>().currentPlayerString = NPC_Manager.GetComponent<NPC_Bubbles>().AnswerAAAA;
                NPC_Manager.GetComponent<NPC_Bubbles>().CurrentDialogue = NPC_Manager.GetComponent<NPC_Bubbles>().DialogueBAB;
                StartCoroutine("PlayerAnswering");
                break;
            default:
                print("Incorrect string.");
                break;
        }

        A.SetActive(false);
        B.SetActive(false);
    }

    public Sprite SaveSprite;

    public void AnswerButton()
    {
        //if(endOfChat == false)
        //{
        A.SetActive(true);
        B.SetActive(true);
            Answer_Button.SetActive(false);
        //}
        //else
        //{
        //    myNPC_Bubbles.AnswerButton.GetComponent<Image>().sprite = SaveSprite;
        //    SaveGame.savegame.Save();
        //    SceneManager.LoadScene("MainMenu");
        //}

    }

    IEnumerator PlayerAnswering()
    {
        myNPC_Bubbles.PlayerChat();
        myNPC_Bubbles.BeginChat();
        yield return new WaitForSeconds(3);


    }

}
