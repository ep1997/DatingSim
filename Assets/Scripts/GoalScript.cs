﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalScript : MonoBehaviour
{

    public GameObject InfoPopUp;
    public Text InfoText;
    public string infoString;
    public int xCorrection;
    public Image MiniIcon;
    public Sprite iconSprite;

    //public static bool queriesHitTriggers = true;

    //private void Start()
    //{
    //    InfoText = InfoPopUp.GetComponent<Text>();
    //}

    public void OnClickGoal()
    {
        InfoPopUp.SetActive(true);
        InfoPopUp.transform.position = new Vector3(transform.position.x + xCorrection, transform.position.y + 200, transform.position.z);
        InfoText.text = infoString;
        MiniIcon.sprite = iconSprite;
    }

    public void OnQuitGoal()
    {
        InfoPopUp.SetActive(false);
    }

}
