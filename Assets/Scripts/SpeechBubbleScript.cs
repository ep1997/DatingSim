﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(ContentSizeFitter))]
public class SpeechBubbleScript : MonoBehaviour {

    public GameObject NPC_Manager;
    public NPC_Bubbles myNPC_Bubbles;
    public string currentResponse;

    public int characterStartSize = 1;

	public float characterAnimateSpeed = 1000f;

	public Image bubbleBackground;

	public float backgroundMinimumHeight;

	public float backgroundVerticalMargin;

	private string _rawText;
	public string rawText {
		get { return _rawText; }
	}

	private string _processedText;
	public string processedText {
		get { return _processedText; }
	}

    public GameObject cam;

    public void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera");

        cam.transform.position = new Vector3(cam.transform.position.x, gameObject.transform.position.y, 0);

        NPC_Manager = GameObject.FindGameObjectWithTag("NPC_Manager");

        myNPC_Bubbles = NPC_Manager.GetComponent<NPC_Bubbles>();

        if(gameObject.tag == ("NPC_Bubble"))
        {
            currentResponse = myNPC_Bubbles.currentText;
        }
        else
        {
            currentResponse = myNPC_Bubbles.currentPlayerString;
        }

        Set(currentResponse);
    }


	/// <param name="text">Text.</param>
	public void Set (string text) {
		StopAllCoroutines();
        
		StartCoroutine(SetRoutine(text));
	}	

	/// <param name="text">Text.</param>
	public IEnumerator SetRoutine (string text) 
	{
		_rawText = text;
        yield return StartCoroutine(TestFit());
		yield return StartCoroutine(CharacterAnimation());
	}

	private IEnumerator TestFit () 
	{

		Text label = GetComponent<Text>();
		ContentSizeFitter fitter = GetComponent<ContentSizeFitter>();

		// change label alpha to zero to hide test fit
		float alpha = label.color.a;
		label.color = new Color(label.color.r, label.color.g, label.color.b, 0f);

        // configure fitter and set label text so label can auto resize height
        label.text = _rawText;

        //Depending on the texts length I set to horizontal fitmode
        //Long texts get the same with but extend vertically
        //Shorter ones get bubbles custom to the texts width
        if (_rawText.Length >= 15)
        {
            //Debug.Log(_rawText.Length);
            fitter.horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
            fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        }
        else
        {
            //Debug.Log(_rawText.Length);
            fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        }



        // need to wait for a frame before label's height is updated
        yield return new WaitForEndOfFrame();
		// make sure label is anchored to center to measure the correct height
		float totalHeight = label.rectTransform.sizeDelta.y;
        float totalWidth = label.rectTransform.sizeDelta.x;
        // (OPTIONAL) set bubble background
        if (bubbleBackground != null)
        {

            bubbleBackground.rectTransform.sizeDelta = new Vector2(totalWidth + 50, totalHeight + 100);
        }



        // now it's time to test word by word
        _processedText = "";
		string buffer = "";
		string line = "";
		float currentHeight = -1f;
		// yes, sorry multiple spaces
		foreach (string word in _rawText.Split(' ')) {
			buffer += word + " ";
			label.text = buffer;
			yield return new WaitForEndOfFrame();
			if (currentHeight < 0f) {
				currentHeight = label.rectTransform.sizeDelta.y;
			}

			line += word + " ";
		}
		_processedText += line;



        //make color opaque again
        label.color = new Color(label.color.r, label.color.g, label.color.b, alpha);
    }

	private IEnumerator CharacterAnimation () 
	{
		// prepare target
		Text label = GetComponent<Text>();
        label.text = _processedText;
        yield return new WaitForEndOfFrame();
    }

}
