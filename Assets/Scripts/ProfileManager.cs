﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProfileManager : MonoBehaviour
{
    public Text HeartCount;
    public Text NameText;
    public string currentAI;
	// Use this for initialization
	void Start ()
    {
        SaveGame.savegame.Load();

        NameText.text = currentAI;

        switch (currentAI)
        {
            case "Jeon":
                HeartCount.text = SaveGame.savegame.heartsJeon.ToString();
                break;
            case "Tae":
                HeartCount.text = SaveGame.savegame.heartsTae.ToString();
                break;
            default:
                print("Incorrect string.");
                break;
        }       
	}

}
