﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MySceneManager : MonoBehaviour {

    public Text NameText;
    public string nameString = "";

    public GameObject PopUp;
    public GameObject Chats;

    public bool isMainMenu;

    [SerializeField]
    GameObject Time;

    [SerializeField]
    Text TimeText;

    public GameObject FinishPopUp;

    public void Start()
    {
        Time = GameObject.FindGameObjectWithTag("Time");
        TimeText = Time.GetComponent<Text>();

        if(isMainMenu == true)
        {
            nameString = "Welcome," + SaveGame.savegame.playerName + "!";
            NameText.text = nameString;

            if (SaveGame.savegame.firstLogin == 0)
            {
                SaveGame.savegame.firstLogin += 1;
                PopUp.SetActive(true);
                SaveGame.savegame.Save();
            }
            else
            {
                Chats.SetActive(true);
            }

            if(SaveGame.savegame.chatsComplete >= 2)
            {
                FinishPopUp.SetActive(true);
            }
        }
    }

    private void Update()
    {
        TimeText.text = System.DateTime.UtcNow.ToString("HH:mm"); // returns "12:48 15 April, 2019"
    }

    public void TaeScene()
    {
        SceneManager.LoadScene(1);
    }

    public void JeonScene()
    {
        SceneManager.LoadScene(2);
    }

    public void MainMenu()
    {
        SaveGame.savegame.Save();
        SceneManager.LoadScene(0);
    }

    public void TaeProfile()
    {
        SceneManager.LoadScene(3);
    }

    public void JeonProfile()
    {
        SceneManager.LoadScene(4);
    }

    public void QuitApp()
    {
        SaveGame.savegame.Save();
        Application.Quit();
    }



    public Text NameInputField;

    public void SetPlayerName()
    {
        SaveGame.savegame.playerName = NameInputField.text;

        SaveGame.savegame.Save();

        nameString = "Welcome," + SaveGame.savegame.playerName + "!";
        NameText.text = nameString;
    }
}
