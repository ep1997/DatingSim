﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveGame : MonoBehaviour {
    //static reference to this script, so I can easily access it from other methods
    public static SaveGame savegame;

    //the messages that was sent last in this conversation
    public string currentPreviewString;

    public string playerName;

    public int firstLogin;

    public int heartsTae;
    public int heartsJeon;

    public int chatsComplete;

	void Awake () {
        //Check if a savegame object already exists in the scene
        if(savegame == null)
        {
            //If it doesnt then this will be the savegame obj
            DontDestroyOnLoad(gameObject);
            savegame = this;
        }
        else if(savegame != this)
        {
            //If one is already in this scene then destroy the second (uneccessary) one
            Destroy(gameObject);
        }

	}

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        //creating an instance of my PlayerData class
        PlayerData data = new PlayerData();
        //making the currentPreviewStrings value in the scene override the value in player data
        data.currentPreviewString = currentPreviewString;
        data.playerName = playerName;
        data.firstLogin = firstLogin;
        data.heartsTae = heartsTae;
        data.heartsJeon = heartsJeon;
        data.chatsComplete = chatsComplete;
        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            //This time we are overriding the ingame value of the string with the one from data
            currentPreviewString = data.currentPreviewString;
            playerName = data.playerName;
            firstLogin = data.firstLogin;
            heartsTae = data.heartsTae;
            heartsJeon = data.heartsJeon;
            chatsComplete = data.chatsComplete;
        }
    }

}

//Class with players saved values
[System.Serializable]
class PlayerData
{
    public string currentPreviewString;

    public string playerName;

    public int firstLogin;

    public int heartsTae;
    public int heartsJeon;

    public int chatsComplete;
}

