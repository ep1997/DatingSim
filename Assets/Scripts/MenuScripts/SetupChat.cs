﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetupChat : MonoBehaviour {
    public static string currentChat = "chat";

    //Open the characters chat scene
	public void OpenChat()
    {
        currentChat += gameObject.name;

        if(currentChat == "chatJeon")
        {
            SceneManager.LoadScene("EntryScene");
        }

        if(currentChat == "chatTae")
        {
            SceneManager.LoadScene("EntryScene");
        }
    }

    //Open the scene with characters profile displayed
    public void OpenProfile_Jeon()
    {
        SceneManager.LoadScene("Profile1");
    }

    public void OpenProfile_Tae()
    {
        SceneManager.LoadScene("Profile1");
    }
}
